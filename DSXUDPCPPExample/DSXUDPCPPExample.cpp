#include <sstream>
#include <fstream>
#include <iostream>
#include <string>
#include <cstring>

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include "jsoncpp/json/json.h"

#pragma comment(lib,"ws2_32.lib") 
//#pragma warning(disable:4996)
enum class CustomTriggerValueMode
{
    OFF = 0,
    Rigid = 1,
    RigidA = 2,
    RigidB = 3,
    RigidAB = 4,
    Pulse = 5,
    PulseA = 6,
    PulseB = 7,
    PulseAB = 8,
    VibrateResistance = 9,
    VibrateResistanceA = 10,
    VibrateResistanceB = 11,
    VibrateResistanceAB = 12,
    VibratePulse = 13,
    VibratePulseA = 14,
    VibratePulsB = 15,
    VibratePulseAB = 16
};

enum class PlayerLEDNewRevision
{
    One = 0,
    Two = 1,
    Three = 2,
    Four = 3,
    Five = 4,
    // Five is Also All On
    AllOff = 5
};

enum class MicLEDMode
{
    On = 0,
    Pulse = 1,
    Off = 2
};

enum class Trigger
{
    Invalid,
    Left,
    Right
};

enum class TriggerMode
{
    Normal = 0,
    GameCube = 1,
    VerySoft = 2,
    Soft = 3,
    Hard = 4,
    VeryHard = 5,
    Hardest = 6,
    Rigid = 7,
    VibrateTrigger = 8,
    Choppy = 9,
    Medium = 10,
    VibrateTriggerPulse = 11,
    CustomTriggerValue = 12,
    Resistance = 13,
    Bow = 14,
    Galloping = 15,
    SemiAutomaticGun = 16,
    AutomaticGun = 17,
    Machine = 18
};

enum class InstructionType
{
    Invalid,
    TriggerUpdate,
    RGBUpdate,
    PlayerLED,
    TriggerThreshold,
    MicLED,
    PlayerLEDNewRevision,
    ResetToUserSettings
};


struct Instruction
{
    InstructionType type;
    std::vector<int> parameters;
};

struct Packet
{
    std::vector<Instruction> instructions;
};

std::string PacketToJson(const Packet& packet)
{
    // Create the root JSON object
    Json::Value root;

    // Add the instructions array to the root object
    Json::Value instructions_array(Json::arrayValue);
    for (const Instruction& instruction : packet.instructions)
    {
        Json::Value instruction_object;
        instruction_object["type"] = static_cast<int>(instruction.type);
        Json::Value parameters_array(Json::arrayValue);
        for (int parameter : instruction.parameters)
        {
            parameters_array.append(parameter);
        }
        instruction_object["parameters"] = parameters_array;

        instructions_array.append(instruction_object);
    }
    root["instructions"] = instructions_array;

    // Convert the root object to a string and return it
    return root.toStyledString();
}

struct OpenedConnection
{
    SOCKET sock;
    struct sockaddr_in server_addr;
};

int ReadIntFromTextFile(const std::string& filepath)
{
    std::ifstream file(filepath);
    if (!file.is_open())
    {
        std::cerr << "Cannot open file " << filepath << std::endl;
        return 0;
    }

    int value;
    file >> value;
    return value;
}

OpenedConnection OpenConnection()
{
    struct sockaddr_in server_addr;

    int port_number = ReadIntFromTextFile("C:\\Temp\\DualSenseX\\DualSenseX_PortNumber.txt");
    WSADATA wsa_data;
    int result = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (result != 0)
    {
        std::cerr << "Cannot init winsock lib: " << result << std::endl;
    }

    // Crea un socket UDP
    SOCKET sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sockfd == INVALID_SOCKET)
    {
        std::cerr << "Cannot create socket: " << WSAGetLastError() << std::endl;
        WSACleanup();
    }
    std::memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_number);
    server_addr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
    //inet_pton(AF_INET,"127.0.0.1",&server_addr.sin_addr);
    OpenedConnection o = {
    };
    o.server_addr = server_addr;
    o.sock = sockfd;
    return o;
}

void SendPacket(const Packet& packet, SOCKET sockfd, struct sockaddr_in server_addr)
{
    std::string json_str = PacketToJson(packet);
    auto sent_bytes = sendto(
        sockfd,
        json_str.c_str(),
        json_str.size(),
        0,
        (struct sockaddr*)&server_addr,
        sizeof(server_addr)
        );
    if (sent_bytes == SOCKET_ERROR)
    {
        std::cerr << "Error sending udp message: " << WSAGetLastError() << std::endl;
        closesocket(sockfd);
        WSACleanup();
    }
}

int main()
{
    OpenedConnection o = OpenConnection();
    Packet packet;
    int controllerIndex = 0;
    packet.instructions = {
        //right trigger will simulate an automatic gun that start at 0, ends at 8 with a frequency of 30
        {InstructionType::TriggerUpdate, {controllerIndex, static_cast<int>(Trigger::Right), static_cast<int>(TriggerMode::AutomaticGun),0,8,30}},
        //left trigger will have a resistance that starts at 3 with a force of 6
        {InstructionType::TriggerUpdate, {controllerIndex, static_cast<int>(Trigger::Left), static_cast<int>(TriggerMode::Resistance),3,6}},
        //led controller goes to green
        {InstructionType::RGBUpdate, {controllerIndex, 0, 255,0}}
    };
    
    std::string json = PacketToJson(packet);
    std::cout << json << std::endl;

    SendPacket(packet, o.sock, o.server_addr);
    closesocket(o.sock);
    WSACleanup();

    return 0;
}
